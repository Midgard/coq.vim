coq.vim
=======

Overview
--------
A simple Vim plug-in for the Coq Proof Assistant.

Description
-----------
This plug-in provides the "Go to cursor" command of CoqIde –
that is, run `coqtop` on all lines up to and including the focused line, and
display the output in a new split window.

Naturally, it also provides filetype support (filename: `*.v`), syntax highlighting and indentation rules.

Usage
-----
This function is provided as Ex command:

	:CoqRunToCursor

There's a key binding `^C` to run Coq, both in normal and in insert mode.

If you want, you can create your own bindings like so:

	autocommand Filetype coq nnoremap ,, :CoqRunToCursor<CR>

Install
-------
If you use Plugged, add this line in your `~/.vimrc` (Vim) or `~/.config/nvim/init.vim` (NeoVim):

	Plug 'https://framagit.org/Midgard/coq.vim.git'

License
-------
This plug-in is placed in the public domain.  
The indent file is a copy of http://www.vim.org/scripts/script.php?script_id=2079 .  
The syntax file is a copy of http://www.vim.org/scripts/script.php?script_id=2063 .  

Authors
-------
- TOUNAI Shouta \<tounai.shouta@gmail.com\>
- Midgard

History
-------
2014 Dec 15: write README.md  
2014 Dec 16: copied indent and syntax files from vim-scripts  
2015 Mar 02: change user name  
2018 Oct 07: change to vertical split and set norelativenumber  
